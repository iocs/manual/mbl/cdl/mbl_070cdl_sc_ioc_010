###################################### ICS HWI ###############################################
#############################  ICS Instrument Library     ####################################
##  PLC Sample Code in VersionDog: ICS_LIBRARY_MASTER_PLC                                   ## 
##  CCDB device types: ICS_SPK_CRM_Ready                                                    ##  
##  EPICS HMI (Block Icons/Faceplates)@ GitLab. Projekt: Cryo / CryogenicsLibrary / CryoLib ##
##                                                                                          ##  
##                   DIO - Cryomodule Ready Interlocks - Motor OK                        ##  
##                                                                                          ##  
##                                                                                          ##  
############################          Version: 1.1         ###################################
# Author:  Wojciech Binczyk
# Date:    22-03-2024
# Version: v1.1 CDS_Cryo_OK moved to Relay singals
############################          Version: 1.0         ###################################
# Author:  Wojciech Binczyk
# Date:    24-06-2023
# Version: v1.0

############################
#  STATUS BLOCK
############################
define_status_block()

#Relay signals
add_digital("OpMode_Auto",             ARCHIVE=True,       PV_DESC="Operation Mode Auto",       PV_ONAM="True",           PV_ZNAM="False")
add_digital("OpMode_Manual",           ARCHIVE=True,       PV_DESC="Operation Mode Manual",     PV_ONAM="True",           PV_ZNAM="False")
add_digital("Opened",                  ARCHIVE=True,       PV_DESC="Opened",                    PV_ONAM="True",           PV_ZNAM="False")
add_digital("Closed",                  ARCHIVE=True,       PV_DESC="Closed",                    PV_ONAM="True",           PV_ZNAM="False")
add_digital("Solenoid",                ARCHIVE=True,       PV_DESC="Solenoid energized",        PV_ONAM="True",           PV_ZNAM="False")
add_digital("CDS_Cryo_OK",             ARCHIVE=True,       PV_DESC="Cryo system OK 2K",         PV_ONAM="True",           PV_ZNAM="False")
add_digital("Relay_Reserve_01",        ARCHIVE=True,       PV_DESC="Reserve",                   PV_ONAM="True",           PV_ZNAM="False")
add_digital("Relay_Reserve_02",        ARCHIVE=True,       PV_DESC="Reserve",                   PV_ONAM="True",           PV_ZNAM="False")
add_digital("Relay_Reserve_03",        ARCHIVE=True,       PV_DESC="Reserve",                   PV_ONAM="True",           PV_ZNAM="False")


#Warm Conditioning conditions
add_digital("TT-019_Temp_OK",          ARCHIVE=True,       PV_DESC="Cavity temperature OK",             PV_ONAM="True",           PV_ZNAM="False")
add_digital("TT-029_Temp_OK",          ARCHIVE=True,       PV_DESC="Cavity temperature OK",             PV_ONAM="True",           PV_ZNAM="False")
add_digital("TT-039_Temp_OK",          ARCHIVE=True,       PV_DESC="Cavity temperature OK",             PV_ONAM="True",           PV_ZNAM="False")
add_digital("TT-049_Temp_OK",          ARCHIVE=True,       PV_DESC="Cavity temperature OK",             PV_ONAM="True",           PV_ZNAM="False")
add_digital("Motor_OK_Res_01_OK",      ARCHIVE=True,       PV_DESC="Reserve",                           PV_ONAM="True",           PV_ZNAM="False")
add_digital("Motor_OK_Res_02_OK",      ARCHIVE=True,       PV_DESC="Reserve",                           PV_ONAM="True",           PV_ZNAM="False")
add_digital("Motor_OK_Res_03_OK",      ARCHIVE=True,       PV_DESC="Reserve",                           PV_ONAM="True",           PV_ZNAM="False")

#Alarm signals
add_major_alarm("Alarm","Alarm",           PV_ZNAM="NominalState")
add_minor_alarm("Warning","Warning",       PV_ZNAM="NominalState")

############################
#  COMMAND BLOCK
############################ 
define_command_block()

#Relay commands
add_digital("Cmd_Auto",                PV_DESC="CMD: Auto Mode")
add_digital("Cmd_Manual",              PV_DESC="CMD: Manual Mode")
add_digital("Cmd_ManuOpen",            PV_DESC="CMD: Manual Open")
add_digital("Cmd_ManuClose",           PV_DESC="CMD: Manual Close")

############################
#  PARAMETER BLOCK
############################ 
define_parameter_block()

#Motor OK Setpoint
add_analog("Motor_OK_Tcav",           "REAL", PV_PREC="2", ARCHIVE=" 1Hz",              PV_DESC="Motor_OK cavity temperature setpoint",           PV_EGU="K")
add_analog("Motor_OK_SP_Res_01",      "REAL", PV_PREC="2", ARCHIVE=" 1Hz",              PV_DESC="Reserve",                                        PV_EGU="K")
add_analog("Motor_OK_SP_Res_02",      "REAL", PV_PREC="2", ARCHIVE=" 1Hz",              PV_DESC="Reserve",                                        PV_EGU="K")
add_analog("Motor_OK_SP_Res_03",      "REAL", PV_PREC="2", ARCHIVE=" 1Hz",              PV_DESC="Reserve",                                        PV_EGU="K")

#Signal Name and Description
add_string("SignalName", 30, PV_VAL="[PLCF#SignalName0]", PV_PINI="YES")
add_string("SignalDesc", 30, PV_VAL="[PLCF#SignalDescription0]", PV_PINI="YES")
