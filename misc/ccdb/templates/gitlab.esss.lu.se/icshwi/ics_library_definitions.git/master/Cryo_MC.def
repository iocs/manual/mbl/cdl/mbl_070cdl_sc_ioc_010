######################################Cryo-WF############################################
################################# Version: 1.0 ##########################################
# Author:  Adalberto Fontoura
# Date:    16-05-2024
# Version: v1.0
# Def file for Master PLC Communication
#########################################################################################

############################
#  STATUS BLOCK
############################
define_status_block()

#Send from Master and Receive in Slaves PLC PVs
add_digital("VacNotOK",                 ARCHIVE=" 1Hz",           PV_DESC="VacuumNotOkay",           PV_ONAM="True",           PV_ZNAM="False")
add_digital("CryoOK",                   ARCHIVE=" 1Hz",           PV_DESC="CryoOK",                  PV_ONAM="True",           PV_ZNAM="False")
add_digital("CryoOKForced",             ARCHIVE=" 1Hz",           PV_DESC="CryoOK signal forced",    PV_ONAM="True",           PV_ZNAM="False")
add_digital("CryoReady",                ARCHIVE=" 1Hz",           PV_DESC="CryoReady",               PV_ONAM="True",           PV_ZNAM="False")
add_digital("CryoReadyForced",          ARCHIVE=" 1Hz",           PV_DESC="CryoReady signal forced", PV_ONAM="True",           PV_ZNAM="False")
add_digital("ACCPStarted",              ARCHIVE=" 1Hz",           PV_DESC="ACCPStarted",             PV_ONAM="True",           PV_ZNAM="False")
add_digital("PurgeDone",                ARCHIVE=" 1Hz",           PV_DESC="PurgeDone",               PV_ONAM="True",           PV_ZNAM="False")
add_digital("ActivateOM1",              ARCHIVE=" 1Hz",           PV_DESC="ActivateOM1",             PV_ONAM="True",           PV_ZNAM="False")
add_digital("ActivateOM2",              ARCHIVE=" 1Hz",           PV_DESC="ActivateOM2",             PV_ONAM="True",           PV_ZNAM="False")
add_digital("ActivateOM3",              ARCHIVE=" 1Hz",           PV_DESC="ActivateOM3",             PV_ONAM="True",           PV_ZNAM="False")
add_digital("ActivateOM4",              ARCHIVE=" 1Hz",           PV_DESC="ActivateOM4",             PV_ONAM="True",           PV_ZNAM="False")
add_digital("ActivateOM5",              ARCHIVE=" 1Hz",           PV_DESC="ActivateOM5",             PV_ONAM="True",           PV_ZNAM="False")
add_digital("ActivateOM6",              ARCHIVE=" 1Hz",           PV_DESC="ActivateOM6",             PV_ONAM="True",           PV_ZNAM="False")
add_digital("ActivateOM7",              ARCHIVE=" 1Hz",           PV_DESC="ActivateOM7",             PV_ONAM="True",           PV_ZNAM="False")
add_digital("ActivateOM8",              ARCHIVE=" 1Hz",           PV_DESC="ActivateOM8",             PV_ONAM="True",           PV_ZNAM="False")
add_digital("ActivateOM9",              ARCHIVE=" 1Hz",           PV_DESC="ActivateOM9",             PV_ONAM="True",           PV_ZNAM="False")
add_digital("ActivateOM10",             ARCHIVE=" 1Hz",           PV_DESC="ActivateOM10",            PV_ONAM="True",           PV_ZNAM="False")
add_digital("ActivateSOM3",             ARCHIVE=" 1Hz",           PV_DESC="ActivateSOM3",            PV_ONAM="True",           PV_ZNAM="False")
add_digital("ActivateSOM5",             ARCHIVE=" 1Hz",           PV_DESC="ActivateSOM5",            PV_ONAM="True",           PV_ZNAM="False")
add_digital("ActivateSOM8",             ARCHIVE=" 1Hz",           PV_DESC="ActivateSOM8",            PV_ONAM="True",           PV_ZNAM="False")
add_digital("ActivateSOM9",             ARCHIVE=" 1Hz",           PV_DESC="ActivateSOM9",            PV_ONAM="True",           PV_ZNAM="False")
add_digital("Pumpdown_started",         ARCHIVE=" 1Hz",           PV_DESC="ACCP:Pumpdown started",   PV_ONAM="True",           PV_ZNAM="False")
add_digital("WU_4K_started",            ARCHIVE=" 1Hz",           PV_DESC="ACCP:4K WU started",      PV_ONAM="True",           PV_ZNAM="False")
add_digital("WU_300K_started",          ARCHIVE=" 1Hz",           PV_DESC="ACCP:300K WU started",    PV_ONAM="True",           PV_ZNAM="False")

#PERMISIVES
#add_digital("WU_4K_lvl_dec",            ARCHIVE=" 1Hz",           PV_DESC="Permisive: lvl dec",          PV_ONAM="True",           PV_ZNAM="False")
#add_digital("WU_300K_lvl_dec",          ARCHIVE=" 1Hz",           PV_DESC="Permisive: lvl dec",          PV_ONAM="True",           PV_ZNAM="False")
add_digital("PerT100",                  ARCHIVE=" 1Hz",           PV_DESC="Permisive for T100",          PV_ONAM="True",           PV_ZNAM="False")
add_digital("PerT199",                  ARCHIVE=" 1Hz",           PV_DESC="Permisive for T199",          PV_ONAM="True",           PV_ZNAM="False")
add_digital("PerT299",                  ARCHIVE=" 1Hz",           PV_DESC="Permisive for T299",          PV_ONAM="True",           PV_ZNAM="False")
add_digital("PerT309",                  ARCHIVE=" 1Hz",           PV_DESC="Permisive for T310",          PV_ONAM="True",           PV_ZNAM="False")
add_digital("PerT399",                  ARCHIVE=" 1Hz",           PV_DESC="Permisive for T399",          PV_ONAM="True",           PV_ZNAM="False")
add_digital("PerT499",                  ARCHIVE=" 1Hz",           PV_DESC="Permisive for T499",          PV_ONAM="True",           PV_ZNAM="False")
add_digital("PerT4991",                 ARCHIVE=" 1Hz",           PV_DESC="Permisive for T4991",         PV_ONAM="True",           PV_ZNAM="False")
add_digital("PerT500",                  ARCHIVE=" 1Hz",           PV_DESC="Permisive for T500",          PV_ONAM="True",           PV_ZNAM="False")
add_digital("PerT503",                  ARCHIVE=" 1Hz",           PV_DESC="Permisive for T503",          PV_ONAM="True",           PV_ZNAM="False")
add_digital("PerT599",                  ARCHIVE=" 1Hz",           PV_DESC="Permisive for T599",          PV_ONAM="True",           PV_ZNAM="False")
add_digital("PerT699",                  ARCHIVE=" 1Hz",           PV_DESC="Permisive for T699",          PV_ONAM="True",           PV_ZNAM="False")
add_digital("PerT701",                  ARCHIVE=" 1Hz",           PV_DESC="Permisive for T701",          PV_ONAM="True",           PV_ZNAM="False")
add_digital("PerT707",                  ARCHIVE=" 1Hz",           PV_DESC="Permisive for T707",          PV_ONAM="True",           PV_ZNAM="False")
add_digital("PerT799",                  ARCHIVE=" 1Hz",           PV_DESC="Permisive for T799",          PV_ONAM="True",           PV_ZNAM="False")
add_digital("PerT801",                  ARCHIVE=" 1Hz",           PV_DESC="Permisive for T801",          PV_ONAM="True",           PV_ZNAM="False")
add_digital("PerT808",                  ARCHIVE=" 1Hz",           PV_DESC="Permisive for T808",          PV_ONAM="True",           PV_ZNAM="False")
add_digital("PerT899",                  ARCHIVE=" 1Hz",           PV_DESC="Permisive for T899",          PV_ONAM="True",           PV_ZNAM="False")
add_digital("PerRes1",                  ARCHIVE=" 1Hz",           PV_DESC="Permisive Reserve1",          PV_ONAM="True",           PV_ZNAM="False")
add_digital("PerRes2",                  ARCHIVE=" 1Hz",           PV_DESC="Permisive Reserve2",          PV_ONAM="True",           PV_ZNAM="False")
add_digital("PerRes3",                  ARCHIVE=" 1Hz",           PV_DESC="Permisive Reserve3",          PV_ONAM="True",           PV_ZNAM="False")
add_digital("PerRes4",                  ARCHIVE=" 1Hz",           PV_DESC="Permisive Reserve4",          PV_ONAM="True",           PV_ZNAM="False")
add_digital("PerRes5",                  ARCHIVE=" 1Hz",           PV_DESC="Permisive Reserve5",          PV_ONAM="True",           PV_ZNAM="False")
add_digital("PerRes6",                  ARCHIVE=" 1Hz",           PV_DESC="Permisive Reserve6",          PV_ONAM="True",           PV_ZNAM="False")
add_digital("PerRes7",                  ARCHIVE=" 1Hz",           PV_DESC="Permisive Reserve7",          PV_ONAM="True",           PV_ZNAM="False")
add_digital("PerRes8",                  ARCHIVE=" 1Hz",           PV_DESC="Permisive Reserve8",          PV_ONAM="True",           PV_ZNAM="False")
add_digital("ACCPTrip",                 ARCHIVE=" 1Hz",           PV_DESC="ACCPTrip",                    PV_ONAM="True",           PV_ZNAM="False")
add_digital("ACCPTripForced",           ARCHIVE=" 1Hz",           PV_DESC="ACCPTrip  signal forced",     PV_ONAM="True",           PV_ZNAM="False")
add_digital("ACCPStop",                 ARCHIVE=" 1Hz",           PV_DESC="ACCPStop",                    PV_ONAM="True",           PV_ZNAM="False")
add_digital("ACCPStopForced",           ARCHIVE=" 1Hz",           PV_DESC="ACCPStop signal forced",      PV_ONAM="True",           PV_ZNAM="False")
add_digital("LossACCP2K",               ARCHIVE=" 1Hz",           PV_DESC="LossACCP2K",                  PV_ONAM="True",           PV_ZNAM="False")
add_digital("LossACCP2KForced",         ARCHIVE=" 1Hz",           PV_DESC="LossACCP2K signal forced",    PV_ONAM="True",           PV_ZNAM="False")
add_digital("LossACCP_SP2K",            ARCHIVE=" 1Hz",           PV_DESC="LossACCP_SP2K",               PV_ONAM="True",           PV_ZNAM="False")
add_digital("LossACCP_SP2KForced",      ARCHIVE=" 1Hz",           PV_DESC="LossACCP_SP2K signal forced", PV_ONAM="True",           PV_ZNAM="False")
add_digital("LossIsoVacCDS",            ARCHIVE=" 1Hz",           PV_DESC="LossIsoVacCDS",               PV_ONAM="True",           PV_ZNAM="False")
add_digital("LossIsoVacCDSForced",      ARCHIVE=" 1Hz",           PV_DESC="LossIsoVacCDS signal forced", PV_ONAM="True",           PV_ZNAM="False")
add_digital("LossIAR",                  ARCHIVE=" 1Hz",           PV_DESC="LossIAR",                     PV_ONAM="True",           PV_ZNAM="False")
add_digital("LossIARForced",            ARCHIVE=" 1Hz",           PV_DESC="LossIAR signal forced",       PV_ONAM="True",           PV_ZNAM="False")
add_analog("MM_ACTIVE_NO", "INT",       ARCHIVE=" 1Hz",           PV_DESC="Master active state")
add_analog("CMDS_Heartbeat", "INT",     ARCHIVE=" 1Hz",           PV_DESC="Master PLC Heartbeat",        PV_ADEL="0")

#Send from Slaves and Receive in Master PLC PV
add_analog("S_ACTIVE_NO", "INT",        ARCHIVE=" 1Hz",           PV_DESC="S_ACTIVE_NO")
add_analog("S_SEL_NO",    "INT",        ARCHIVE=" 1Hz",           PV_DESC="S_SEL_NO")
add_analog("OM_SEL_NO",   "INT",        ARCHIVE=" 1Hz",           PV_DESC="OM_SEL_NO")

add_digital("ACTIVE",                   ARCHIVE=" 1Hz",           PV_DESC="ACTIVE",                PV_ONAM="True",           PV_ZNAM="False")
add_digital("AUTO_ON",                  ARCHIVE=" 1Hz",           PV_DESC="AUTO_ON",               PV_ONAM="True",           PV_ZNAM="False")
add_digital("MAN_ON",                   ARCHIVE=" 1Hz",           PV_DESC="MAN_ON",                PV_ONAM="True",           PV_ZNAM="False")
add_digital("ERR_FLT",                  ARCHIVE=" 1Hz",           PV_DESC="ERR_FLT",               PV_ONAM="True",           PV_ZNAM="False")
add_digital("Interlock",                ARCHIVE=" 1Hz",           PV_DESC="Interlock",             PV_ONAM="True",           PV_ZNAM="False")
add_digital("Supervision",              ARCHIVE=" 1Hz",           PV_DESC="Supervision",           PV_ONAM="True",           PV_ZNAM="False")
add_time("S_Time",                      ARCHIVE=" 1Hz",           PV_DESC="S_Time")
add_digital("S_Delay",                  ARCHIVE=" 1Hz",           PV_DESC="S_Delay",               PV_ONAM="True",           PV_ZNAM="False")
add_digital("S_Timeout",                ARCHIVE=" 1Hz",           PV_DESC="S_Timeout",             PV_ONAM="True",           PV_ZNAM="False")
add_time("S_Delay-S",                   ARCHIVE=" 1Hz",           PV_DESC="S_Delay-S")
add_time("S_Timeout-S",                 ARCHIVE=" 1Hz",           PV_DESC="S_Timeout-S")
add_digital("Trans_Satisfied",          ARCHIVE=" 1Hz",           PV_DESC="Tran cond OK",          PV_ONAM="True",           PV_ZNAM="False")
add_digital("Error_Supervis",           ARCHIVE=" 1Hz",           PV_DESC="Error supervision",     PV_ONAM="True",           PV_ZNAM="False")
add_digital("Error_Timeout",            ARCHIVE=" 1Hz",           PV_DESC="Error timeout",         PV_ONAM="True",           PV_ZNAM="False")
add_digital("CRYOMODULE_READY",         ARCHIVE=" 1Hz",           PV_DESC="CRYOMODULE_READY",      PV_ONAM="True",           PV_ZNAM="False")
add_digital("MOTOR_OK",                 ARCHIVE=" 1Hz",           PV_DESC="MOTOR_OK",              PV_ONAM="True",           PV_ZNAM="False")
add_digital("BEAMVAC_OK",               ARCHIVE=" 1Hz",           PV_DESC="BEAMVAC_OK",            PV_ONAM="True",           PV_ZNAM="False")
add_digital("ISOVAC_OK",                ARCHIVE=" 1Hz",           PV_DESC="ISOA/C_OK",             PV_ONAM="True",           PV_ZNAM="False")
add_digital("VACUUM_OK",                ARCHIVE=" 1Hz",           PV_DESC="VACUUM_OK",             PV_ONAM="True",           PV_ZNAM="False")
add_digital("Devices_In_Auto",          ARCHIVE=" 1Hz",           PV_DESC="Devices_In_Auto",       PV_ONAM="True",           PV_ZNAM="False")
add_digital("All_At_Zero",              ARCHIVE=" 1Hz",           PV_DESC="All_At_Zero",           PV_ONAM="True",           PV_ZNAM="False")
add_digital("Unctrl_Devices_OK",        ARCHIVE=" 1Hz",           PV_DESC="All_At_Zero",           PV_ONAM="True",           PV_ZNAM="False")
add_digital("Devices_NoAlarm",          ARCHIVE=" 1Hz",           PV_DESC="All_At_Zero",           PV_ONAM="True",           PV_ZNAM="False")
add_digital("Purge_done_OK",            ARCHIVE=" 1Hz",           PV_DESC="All_At_Zero",           PV_ONAM="True",           PV_ZNAM="False")
add_digital("Combined_CD",              ARCHIVE=" 1Hz",           PV_DESC="All_At_Zero",           PV_ONAM="True",           PV_ZNAM="False")
add_digital("Single_Crm",               ARCHIVE=" 1Hz",           PV_DESC="Singled CM ctrl",       PV_ONAM="True",           PV_ZNAM="False")
add_digital("Multi_Crm",                ARCHIVE=" 1Hz",           PV_DESC="Multiple CMs ctrl",     PV_ONAM="True",           PV_ZNAM="False")
add_digital("Rdy_for_pumpdown",         ARCHIVE=" 1Hz",           PV_DESC="CM rdy for pumpdown",   PV_ONAM="True",           PV_ZNAM="False")
add_digital("Rdy_for_4K_WU",            ARCHIVE=" 1Hz",           PV_DESC="CM rdy for 4K WU",      PV_ONAM="True",           PV_ZNAM="False")
add_digital("Rdy_for_300K_WU",          ARCHIVE=" 1Hz",           PV_DESC="CM rdy for 300K WU",    PV_ONAM="True",           PV_ZNAM="False")
add_digital("LossIsoVac",               ARCHIVE=" 1Hz",           PV_DESC="All_At_Zero",           PV_ONAM="True",           PV_ZNAM="False")
add_digital("LossBeamVac",              ARCHIVE=" 1Hz",           PV_DESC="All_At_Zero",           PV_ONAM="True",           PV_ZNAM="False")
add_digital("CavQuen",                  ARCHIVE=" 1Hz",           PV_DESC="All_At_Zero",           PV_ONAM="True",           PV_ZNAM="False")
add_digital("ParkedCrm",                ARCHIVE=" 1Hz",           PV_DESC="Parked Cryomodule",                PV_ONAM="True",           PV_ZNAM="False")

#Communication control
add_analog("Heartbeat", "INT",          ARCHIVE=" 1Hz",           PV_DESC="Device Heartbeat",       PV_ADEL="0")
add_digital("ActiveConn",               ARCHIVE=" 1Hz",           PV_DESC="Send connection established",                      PV_ONAM="True",           PV_ZNAM="False")
add_digital("ConnError",                ARCHIVE=" 1Hz",           PV_DESC="Send connection error",                            PV_ONAM="True",           PV_ZNAM="False") 
add_analog("ConnErrorStatus", "WORD",                             PV_DESC="Send connection error")
add_analog("PNDiag_Mtnc_Stt", "WORD",                             PV_DESC="PN Diag Maintenance State")
add_analog("PNDiag_Cmp_Stt", "WORD",                              PV_DESC="PN Diag Component State Detail")
add_analog("PNDiag_Own_Stt", "WORD",                              PV_DESC="PN Diag Own State")
add_analog("PNDiag_IO_Stt", "WORD",                               PV_DESC="PN Diag IO State")
add_analog("PNDiag_Oprt_Stt", "WORD",                             PV_DESC="PN Diag Operating State")
add_digital("SND_Done",                                           PV_DESC="Data sent",                                        PV_ONAM="True",           PV_ZNAM="False") 
add_digital("SND_Error",                ARCHIVE=" 1Hz",           PV_DESC="Data sent error",                                  PV_ONAM="True",           PV_ZNAM="False") 
add_digital("SND_Busy",                                           PV_DESC="Send operation busy",                              PV_ONAM="True",           PV_ZNAM="False") 
add_digital("RCV_Busy",                                           PV_DESC="Receive operation busy",                           PV_ONAM="True",           PV_ZNAM="False")
add_digital("RCV_Done",                                           PV_DESC="Receive operation Done",                           PV_ONAM="True",           PV_ZNAM="False")
add_digital("RCV_Error",                ARCHIVE=" 1Hz",           PV_DESC="Receive operation Error",                          PV_ONAM="True",           PV_ZNAM="False")

#Alarm signals
add_major_alarm("CommError",            "CommError",                                                PV_ZNAM="NominalState")
add_major_alarm("MasterCommError",      "MasterCommError",                                          PV_ZNAM="NominalState")
add_major_alarm("SlaveCommError",       "SlaveCommError",                                           PV_ZNAM="NominalState")


############################
#  COMMAND BLOCK
############################
define_command_block()

#OPI buttons
add_digital("Cmd_Rst_Rcv_com",           ARCHIVE=" 1Hz",		  PV_DESC="CMD: Reset receiving operation")
add_digital("Cmd_Rst_Snd_com",           ARCHIVE=" 1Hz",		  PV_DESC="CMD: Reset Sending operation")

############################
#  PARAMETER BLOCK
############################
define_parameter_block()

############################
#  MESSAGE BLOCK
############################







